﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AliExpressPoc.AppCode
{
    class ProcessCsv
    {
        /// <summary>
        /// this method is used to read url from csv file
        /// </summary>
        /// <returns></returns>
        public static List<string> ReadUrl(List<string> FileNames)
        {
            Console.WriteLine("System Reading Url from Csv file...");
            Log.Write("System Reading Url from Csv file...");
            List<string> Data = new List<string>();
            foreach (string FileName in FileNames)
            {
                try
                {
                    using (var fs = File.OpenRead(AppDomain.CurrentDomain.BaseDirectory + @"\CSV\" + FileName))
                    {
                        using (var reader = new StreamReader(fs))
                        {
                            string line = "";
                            while (!reader.EndOfStream)
                            {
                                try
                                {
                                    line = reader.ReadLine();
                                    //var a = HttpUtility.UrlEncode(line);
                                    //var a = Uri.EscapeUriString(line);
                                    //line = line.Replace("\"", "");
                                    line = Uri.EscapeUriString(line);
                                    if (line == "URL") // the first row of csv file has not URL it has the text "URL" that is skiped here
                                        continue;
                                    Data.Add(line);
                                }
                                catch (Exception Ex)
                                {
                                    Log.Write("Error Occurred: " + Ex.Message);
                                    Console.WriteLine("Error Occurred: " + Ex.Message);
                                }
                            }
                        }
                    }
                }
                catch (Exception Ex)
                {
                    Log.Write("Error Occurred: " + Ex.Message);
                    Console.WriteLine("Error Occurred: " + Ex.Message);
                }
            }
            foreach (string FileName in FileNames)
            {
                File.Move(AppDomain.CurrentDomain.BaseDirectory + @"\CSV\" + FileName, AppDomain.CurrentDomain.BaseDirectory + @"\ProccessedCSV\" + String.Format("{0:yyyy-MM-dd hhmmss }", DateTime.Now) + FileName);
            }
            Console.WriteLine("Reading url from csv file completed.");
            Log.Write("Reading url from csv file completed.");
            return Data;
        }

        /// <summary>
        /// this method is used to write product details into the csv file
        /// </summary>
        /// <param name="Data"></param>
        public static void WriteProductDetails(List<ProductDetails> Data)
        {
            List<string> headers = new List<string>();
            List<string> Rows;
            StringBuilder sb = new StringBuilder();
            try
            {
                foreach (var prop in typeof(ProductDetails).GetProperties())
                {
                    headers.Add(prop.Name);
                }
                sb.AppendLine(string.Join(",", headers));

                foreach (ProductDetails item in Data)
                {
                    Rows = new List<string>();
                    foreach (var prop in typeof(ProductDetails).GetProperties())
                    {
                        try
                        {
                            Rows.Add(prop.GetValue(item).ToString());
                        }
                        catch (Exception Ex)
                        {
                            Rows.Add("");
                            Log.Write("Error Occurred: " + Ex.Message);
                            Console.WriteLine("Error Occurred: " + Ex.Message);
                        }
                    }
                    sb.AppendLine(string.Join(",", Rows));
                }
                File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + @"\CSVExported\CSVExported " + String.Format("{0:yyyy-MM-dd hhmmss}", DateTime.Now) + ".csv", sb.ToString());
            }
            catch (Exception Ex)
            {
                Log.Write("Error Occurred: " + Ex.Message);
                Console.WriteLine("Error Occurred: " + Ex.Message);
            }
        }

    }
}
