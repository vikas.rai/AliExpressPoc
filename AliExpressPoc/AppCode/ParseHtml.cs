﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Configuration;
namespace AliExpressPoc.AppCode
{
    class ParseHtml
    {
        /// <summary>
        /// this method is used to parse the html from given url and return list of product details
        /// </summary>
        /// <param name="Urls"></param>
        /// <returns></returns>
        public static List<ProductDetails> ParseHtmlThread(List<string> Urls)
        {
            HtmlWeb web = new HtmlWeb();
            List<ProductDetails> Data = new List<ProductDetails>();
            ProductDetails ProductDetail;

            foreach (string url in Urls)
            {
                try
                {
                    ProductDetail = new ProductDetails();
                    HtmlAgilityPack.HtmlDocument document = web.Load(url);
                    
                    // parsing data from html
                    HtmlNode nodeTitle = document.DocumentNode.SelectNodes(System.Configuration.ConfigurationManager.AppSettings["Title"]).First();
                    HtmlNode nodeImgUrl = document.DocumentNode.SelectNodes(System.Configuration.ConfigurationManager.AppSettings["ImgUrl"]).First();
                    HtmlNode nodeProductDescription = document.DocumentNode.SelectNodes(System.Configuration.ConfigurationManager.AppSettings["ProductDescription"]).First();
                    HtmlNode[] nodePrice = document.DocumentNode.SelectNodes(System.Configuration.ConfigurationManager.AppSettings["Price"]).ToArray();
                    HtmlNode[] nodeWeightNSize = document.DocumentNode.SelectNodes(System.Configuration.ConfigurationManager.AppSettings["WeightNSize"]).ToArray();
                    HtmlNode nodeStarRating = document.DocumentNode.SelectNodes(System.Configuration.ConfigurationManager.AppSettings["StarRating"]).First();
                    HtmlNode nodeNoOfOrders = document.DocumentNode.SelectNodes(System.Configuration.ConfigurationManager.AppSettings["NoOfOrders"]).First();

                    // set data into ProductDetail object
                    
                    var u = Uri.EscapeUriString(new Uri(new Uri("http://aliexpressapi.com/"), nodeImgUrl.Attributes["src"].Value).AbsoluteUri);
                    ProductDetail.SrNo = 0;
                    ProductDetail.Title = nodeTitle.InnerText;
                    ProductDetail.ImgUrl = new Uri(new Uri("http://aliexpressapi.com/"), nodeImgUrl.Attributes["src"].Value).AbsoluteUri.Replace("&#39;", "'").Replace("&amp;", "&");
                    ProductDetail.ProductDescription = HttpUtility.HtmlDecode(nodeProductDescription.InnerText).ToString();
                    //nodeProductDescription.InnerText.Replace(",", ";").Replace("&nbsp;", " ").Replace("&gt;", ">").Replace("&lt;", "<").Replace("&amp;", "&"));

                    ProductDetail.Price = nodePrice[0].InnerText; // nodePrice is an array and it has price and DiscountedPrice
                    if (nodePrice.Length > 1) // checking that the node has DiscountedPrice or not(means the array has two elements or not
                        ProductDetail.DiscountedPrice = nodePrice[1].InnerText;
                    else
                        ProductDetail.DiscountedPrice = ""; // if it has not DiscountedPrice then set it with bkank

                    ProductDetail.Weight = nodeWeightNSize[1].InnerText; // nodeWeightNSize is an array and it has 6 element and second is weight and third is size
                    if (nodeWeightNSize.Length > 2) // checking that the node has element more then 3 or not
                        ProductDetail.size = nodeWeightNSize[2].InnerText;
                    else
                        ProductDetail.size = ""; // if it has not size then set it with bkank

                    ProductDetail.StarRating = nodeStarRating.InnerText.Replace("\n", "").Trim();
                    ProductDetail.NoOfOrders = nodeNoOfOrders.InnerText;
                    Data.Add(ProductDetail);//adding ProductDetail into list of ProductDetail
                }
                catch (Exception Ex)
                {
                    Log.Write("Error Occurred: " + Ex.Message + "/n At Url: " + url);
                    Console.WriteLine("Error Occurred: " + Ex.Message + "/n At Url: " + url);
                }
            }
            return Data;
        }

    }
}
