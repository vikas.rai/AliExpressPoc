﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliExpressPoc.AppCode
{
    class Log
    {
        public static void Write(string Message)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + @"\ErrorLog\LogFile.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ": " + Message);
                sw.Flush();
                sw.Close();
            }
            catch (Exception Ex)
            {
                Console.WriteLine("Error Occurred into writing log : " + Ex.Message + "/n At Url:");
            }

        }
    }
}
